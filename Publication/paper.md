---
title: 'GenomOrder: A Nextflow tool for Alignment visualization and Scaffolding'
tags:
  - Nextflow
  - Python
  - Genome assembly
  - Genome alignment
  - Genome scaffolding
authors:
  - name: Clément Birbes^[co-first author]
    orcid: Entrer Orcid ID
    affiliation: 1
  - name: Christophe Klopp^[co-first author]
    affiliation: 1
  - name: Claire Kuchly^[co-first author]
    affiliation: 2
  - name: Les autres
    affiliation: "1, 2, 3, 4"
affiliations:
  - name: Plate-forme bio-informatique Genotoul, Mathématiques et Informatique Appliquées de Toulouse, INRA, Castanet Tolosan, France
    index: 1
  - name: Get-Plage (Modifier pour nom exact)
    index: 2
date: 04 August 2021
bibliography: paper.bib

# Summary

GenomOrder is a pipeline comprising two modules : genomic assembly reorganisation and in between assemblies reference comparision. Given a reference assembly, genomorder will reorder, reorient and rename sequences from up to five assemblies according to the reference assembly. If the reference assembly is in chromosomes and the other assemblies are not, genomeorder can scaffold the assemblies in chromosomes. Given a list of chromosomes, genomorder will align the chromosomes, sharing the same name, coming from the different assemblies with each other and produce visualisation archives for http://dgenies.toulouse.inra.fr/. 

# Statement of need

With the availability of cheap long read sequences and efficient genome assembly software packages, novel genome assemblies are made available frequently. It is not rare to produce multiple references of the same or closely related species in a project. These assemblies being produced independently, raw assembly file include chromosomes or scaffolds in random order, orientation and with random sequence names. Depending on the availability of genomic long range information (Hi-C, optical maps, linked reads) the assemblies can sometime not have been brought to chromosomes. Scaffolding these assemblies using a reference genome is one option to bring further the assembly state. For related species, once assemblies are in the same configuration, scientist are interested in comparing them at the chromosome level. Here chromosome dot plots are the first broad approach to find large genomic rearrangements. 

# Genomorder architecture 

Genomorder is a Nextflow pipeline including seven python modules taking care of splitting the assembly fasta file in chromosomes...
Genomorder adapts to the available computing architecture through the use of a Nextflow profile file which contains the setting of the different available processing queues when using a HPC scheduler or the characterics of the local computer. 

# Citations

# Figures

![Dot plot produced from the d-genies archive file generate by Genomorder for CM010541 and a de novo assembly using the reads of CM010541.\label{fig:architecture}](https://forgemia.inra.fr/seqoccin/GenomOrder/-/raw/master/Image/HowTo_4.svg)

# Acknowledgements

This work have been funded by the SeqOccIn project (Sequencing Occitanie Innovation, https://get.genotoul.fr/seqoccin/) aiming at acquiring expertise on the optimal combination of long fragment sequencing technologies and associated applications to better characterize complex genomes for species of agronomical interest (maize, sheep, cow).
