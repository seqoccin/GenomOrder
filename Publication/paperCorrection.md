---
title: 'GenomOrder: A Nextflow Alignment visualization and Scaffolding pipeline'
tags:
  - Nextflow
  - Genome assembly
  - Genome alignment
  - Genome scaffolding
  - Dot-plots
authors:
  - name: Clément Birbes^[co-first author]
    orcid: Entrer Orcid ID
    affiliation: 1
  - name: Christophe Klopp^[co-first author]
    affiliation: 1
  - name: Claire Kuchly^[co-first author]
    affiliation: 2
  - name:
    affiliation: "1, 2, 3, 4"
affiliations:
  - name: Plateforme Bio-informatique Genotoul, Mathématiques et Informatique Appliquées de Toulouse, INRAE, Castanet-Tolosan, France
    index: 1
  - name: INRAE, US 1426, GeT-Plage, Genotoul, Castanet-Tolosan, France
    index: 2
date: 04 August 2021
bibliography: genomorder.bib
---
# Summary

With the availability of cheap long read sequences and [@Catchen2020] efficient genome assembly software packages, novel genome assemblies are made available frequently. It is not rare to produce multiple assemblies of the same or closely related species in a project. These assemblies being produced independently, raw assembly file include contigs, scaffolds or chromosomes in random order, orientation and with different sequence names. Depending on the availability of genomic long range information (Hi-C, optical maps, linked reads) the assemblies are sometimes not at chromosome scale. One option to bring them further towards chromsomal state is to scaffold them using a reference genome. In addition, scientist are interested in comparing assemblies with each others, often focusing on the chromosome level to find large genomic rearrangements. Dot-plots are a simple and efficient approach to find these rearrangements.
In both cases, GenomOrder can also scaffold and generate chromosomal dot-plots in a fast and simple way.
The pipeline is implemented as Nextflow pipeline and can be run with Docker, code is available on Github.

# Statement of need

Today, novel genome assemblies often organized as chromosomes. If a reference genome is already available, common practice is to follow its naming, sorting and orienting schemas for novel assemblies. If multiple assemblies are produced, this has to be performes for each of them. GenomOrder aims at easing organizing assemblies as a reference. It includes reference base scaffolding when the assemblies are not already in chromosomes as well as chrosmosome scale dot plot visualization. GenomOrder reference scaffolding and visualisation is based on DGenies [Cabanettes:2018]. GenomOrder is quick and easy to use.

# Material and Methods

## Features

This pipeline is implemented in Nextflow, a portable, reproducible, scalable and parallelizable workflow framework [@Di:2017]. With Nextflow, GenomOrder pipeline is designed to parallelize and automate a list of process in a single command line, thus improving reproducibility and traceability while allowing rapid production. In addition, as the pipeline is developped with multiple features, it allow users to customize command-line options to fit the desired behaviour.

GenomOrder is developped with two main modules : genomic assembly reorganisation and assemblies reference comparision.
GenomOrder can reorder, reorient and rename sequences from up to five assemblies according to a given reference. If the reference assembly is in chromosomes and the other assemblies are not, genomeorder can scaffold the assemblies in chromosomes.
Given a list of chromosomes, GenomOrder will align the chromosomes sharing the same name and produce an all-vs-all chromosome visualisation archives for http://dgenies.toulouse.inra.fr/ [FIGURE X]. In addition, GenomOrder can simply align multiple assemblies against a given reference assembly and quickly produce dot-plot archive for http://dgenies.toulouse.inra.fr/ [FIGURE Y].


## Workflow

[FIGURE Z]
  1. Input. GenomOrder requires at least one assembly fasta file and one reference fasta file to produce an alignment and the resulting DGenies visualization files. Optionally, users can provide up to 4 other assemblies to be aligned to the reference. One option allows users to scaffold the input assemblies against the reference, and a other one aligns chromosome having the same name to produce dot plots.

  2. Align and produce DGenies backup files. Assemblies are aligned to the reference using minimap 2. Fasta file are then indexed and alignment file is sorted to produce an archive that can be given as input to DGenies for the dot-plot visualization.

  3. Arrange assembly. If input parameters '--arrange' is set to True, input assembly can be reordered in accordance with the reference. Index, fasta and alignment file are used through a Python script that reorganize the input assembly.

  4. Compare chromosomes. If a list of chromosome to compare is give as input with '--compare', these chromosome are find in each given assemblies and aligned against each others. Producing the DGenies archives.

## Configuration file

Four configuration and file containing pipeline options and parameters are provided. Users can customize files to change allocated memory or CPU for each process. Advanced users can modify any parameters in main file to deploy new process and upgrade the pipeline.

## Installation and execution

GenomOrder can be cloned from https://forgemia.inra.fr/seqoccin/GenomOrder (A CHANGER SI ON PASSE SUR GITHUB GENOTOUL). A Docker container is available to help running the pipeline without further dependency installation. The pipeline can be run localy or in an high-performance computing environments (cluster). If all specific dependencies are installed, pipeline can be run without the docker container.
Further information about how to run the pipeline are available on github, in the config file, or with the '--help' command.

## Output and Error Handling

Output files are stored in the ouput folder specified with the '--output' parameter. Each new folder contains only the principal output. Nextflow creates its own work folder to produce the intermediate output. Process logs are stored in a specific folder for each run. If the run stops due to an error, user can fix this error and then run the pipeline with the initial command and '-resume' option.

## Conclusion and discussions

The GenomOrder pipeline is user-friendly and provides a one-step analysis tool. Required options and parameters are limited and easily understandable. In addition, it will be easily possible to implement new options and functionalities requested by users, in order to satisfy their needs.

# References

# Figures

![Workflow pipeline](A renseigner)
![Dot plot DGenies, comparaison de chromosomes identiques dans differents assemblages](A renseigner)
![Dot plot produced from the d-genies archive file generate by Genomorder for CM010541 and a de novo assembly using the reads of CM010541.\label{fig:architecture}](https://forgemia.inra.fr/seqoccin/GenomOrder/-/raw/master/Image/HowTo_4.svg)

# Acknowledgements

This work have been funded by the SeqOccIn project (Sequencing Occitanie Innovation, https://get.genotoul.fr/seqoccin/) aiming at acquiring expertise on the optimal combination of long fragment sequencing technologies and associated applications to better characterize complex genomes for species of agronomical interest (maize, sheep, cow).
